import {useState} from "react";
import Button from './components/Button.js';
import Modal from './components/Modal.js';
import './styles.scss'; 

function App() {
  const [isFirstOpen, setFirstModal] = useState(false);
  const [isSecondOpen, setSecondModal] = useState(false);

  const toggleFirst = () => {
    setFirstModal(!isFirstOpen);
  };
  const toggleSecond = () => {
    setSecondModal(!isSecondOpen);
  };

  const firstActions = (
    <div>
      <Button className="modalBtn" text="Ok" onClick={toggleFirst}/>
      <Button className="modalBtn" text="Cancel" onClick={toggleFirst}/>
    </div>
  );

  const secondActions = (
    <div>
      <Button className="modalBtn" text="Send" onClick={toggleSecond}/>
      <Button className="modalBtn" text="Do Something" onClick={toggleSecond}/>
    </div>
  );

  return (
    <div>
    <Button backgroundColor="blue" text="Open first modal" onClick={toggleFirst} />
    <Button backgroundColor="green" text="Open second modal" onClick={toggleSecond} />
    <Modal
        header="Do you want to delete this file?"
        closeButton={true}
        text="Once you delete this file, it won't be possible to undo this action.
        Are you sure you want to delete it?"
        actions={firstActions}
        isOpen={isFirstOpen}
        onClose={toggleFirst}
      />
    <Modal
        header="Second modal header"
        closeButton={true}
        text="Main text of second modal."
        actions={secondActions}
        isOpen={isSecondOpen}
        onClose={toggleSecond}
      />
    </div>
  );
}

export default App;