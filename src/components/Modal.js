const Modal = ({ header, closeButton, text, actions, isOpen, onClose }) => {
    const openModal = {display: isOpen ? 'block' : 'none'};
    const overlayAdd = {display: isOpen ? 'block' : 'none'};
    return (
        <div>
            <div style={overlayAdd} className="addOverlay" onClick={onClose}></div>
            <div style={openModal} className="modal">
                <div className="content">
                    <div className="header"><h2>{header}</h2>
                        {closeButton && (<span className="close-btn" onClick={onClose}>&#x2715;</span>)}
                    </div>
                    <p className="mainText">{text}</p>
                    <div>{actions}</div></div></div>
        </div>
    );};

export default Modal;